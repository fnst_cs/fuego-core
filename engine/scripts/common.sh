# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains common utility functions


# prepend running python with ORIG_PATH if it exist
function run_python() {
    if [ ! -z $ORIG_PATH ]
    then
        echo "running python with PATH=$ORIG_PATH"
        PATH=$ORIG_PATH python "$@"
    else
        python "$@"
    fi
}

function run_python_quiet() {
    if [ ! -z $ORIG_PATH ]
    then
        PATH=$ORIG_PATH python "$@"
    else
        python "$@"
    fi
}

function abort_job {
# $1 - Abort reason string

  set +x
  echo -e "\n*** ABORTED ***\n"
  [ -n "$1" ] && echo -e "Fuego error reason: $1\n"

  # BUILD_URL should be defined. But if not, use a default
  if [ -z "$BUILD_URL" ] ; then
    BUILD_URL="http://localhost:8080/fuego/job/$JOB_NAME/$BUILD_NUMBER"
  fi
  wget -qO- "${BUILD_URL}/stop" > /dev/null
  sleep 15
  echo -e "Jenkins didn't stop the job - quit ourselves"

  # note that we don't send a job 'kill' operation to Jenkins
  # we might already be in post_test, but depending on the size
  # of the logs it might take a long time to retrieve them from the
  # target.  Just exit here, and let the signal handlers do their thing.
  exit 1
}

# check is variable is set and fail if otherwise
function assert_define () {
    varname=$1
    if [ -z "${!varname}" ]
    then
        abort_job "$1 is not defined. Make sure you use correct overlay with required specs for this test/benchmark"
    fi
}

# check if $1 starts with $2
function startswith {
    if [ "${1:0:${#2}}" = "$2" ]  ; then
        return 0
    fi
    return 1
}

TEST_HOME="$FUEGO_CORE/engine/tests/${TESTDIR}"
TRIPLET="${JOB_NAME}-$PLATFORM"

assert_define "FUEGO_CORE"
assert_define "FUEGO_RO"
assert_define "FUEGO_RW"

if [ -z "${BUILD_TIMESTAMP}" ] ; then
  export BUILD_TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)
fi
if [ -z "${FUEGO_START_TIME}" ] ; then
  start_arr=( $(date +"%s %N") )
  # save start time in milliseconds since the epoch
  # (convert nanoseconds returned by 'date' command to milliseconds)
  export FUEGO_START_TIME="${start_arr[0]}${start_arr[1]:0:3}"
fi
