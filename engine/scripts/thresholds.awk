# Sample usage:
#
# gawk -v fw_ver='0\\.18\\.04\\.0001' [ -v start_build=214 -v end_build=220 ] \
#   -f scripts/thresholds.awk \
#   logs/Benchmark.lmbench2/plot.data \
#   ref_logs/Benchmark.lmbench2/lmbench3.log > lmbench3.log.new

BEGIN {
  if (fw_ver == "")
    fw_ver = "."
  if (start_build == "")
    start_build = 0
  if (end_build == "")
    end_build = 9999999999
  count = 0
}

# store threshold operators (le or ge)
/^[[]/ {
    split($1, th_def, "[][|]")
    #print "1:" th_def[2], th_def[3]
    th[th_def[2]] = th_def[3]
    next
}

# store all values for a number of latest builds
$6 ~ fw_ver && $2+0 >= start_build && $2+0 <= end_build && $1 ~ "^20[-0-9_]+$" {
    num = $2
    name = $3
    val = $5
    #print "2:" num, name, val, $6
    builds[num, name] = val
    if (first_build == "")
        first_build = num
    if (last_build != num) {
      last_build = num
      count++
    }
    next
}

END {
    # Determine minimax values across all stored builds
    for (i in builds) {
        split(i, idx, SUBSEP)
        num = idx[1]
        name = idx[2]
        val = builds[num, name]
        if (minimax[name] == "") {
            minimax[name] = val
        }
        else if (th[name] == "le") {
            if (val > minimax[name])
                minimax[name] = val
        }
        else if (th[name] == "ge") {
            if (val < minimax[name])
                minimax[name] = val
        }
        else {
            print "Unsupported threshold operator:", th[name]
            exit 1
        }
    }

    # ...and print adjusted thresholds based on these values.
    # (Output format is compatible with ref_logs.)

    print "# Automatically generated on " ENVIRON["HOSTNAME"] " host by thresholds.awk based on"
    print "# " count " results of " fw_ver " firmware regexp crossed with [" first_build ", " last_build "] build range"

    # Array scanning order could be controlled starting with gawk 4.0.0 like this:
    #PROCINFO["sorted_in"] = "@ind_str_asc" # use predictable array scanning order
    # But for now, we have to use asorti().
    n = asorti(th, th_sorted)
    for (i = 1; i <= n; i++) {
        name = th_sorted[i]
        print "[" name "|" th[name] "]"
        # minimax +/- 5%
        if (th[name] == "le")
            print minimax[name] * 1.05
        else if (th[name] == "ge")
            print minimax[name] * 0.95
        else {
            print "Unsupported threshold operator:", th[name]
            exit 1
        }
    }
}
