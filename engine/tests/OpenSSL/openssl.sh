function test_pre_check {
    assert_define "SDKROOT"
    assert_define "CC"
    assert_define "AR"
    assert_define "RANLIB"
}

function test_build {
    SUFFIX=" --sysroot=${SDKROOT}"

    sed -i -e "s#CC= cc#CC= ${CC}#g" -e "s#AR=ar#AR= ${AR}#g" Makefile
    sed -i -e "s#CFLAGS= #CFLAGS= ${SUFFIX}#g"  Makefile
    sed -i -e "s#ARD=ar#ARD= ${AR}#g" -e "s#RANLIB= /usr/bin/ranlib#RANLIB= ${RANLIB}#g" -e "s#CFLAG= #CFLAG= ${SUFFIX} #g" Makefile
    sed -i -e "s#CC=		cc#CC= ${CC}#g" -e "s#CFLAG=		#CFLAG= ${SUFFIX} #g"  apps/Makefile
    sed -i -e "s#CC=		cc#CC= ${CC}#g" -e "s#AR=		ar#AR= ${AR}#g" -e "s#CFLAG=		#CFLAG= ${SUFFIX} #g" crypto/Makefile
    sed -i -e "s#CC=cc#CC= ${CC} #g"  Makefile.shared

    make
    echo '#!/bin/bash
    cd test
    ../util/opensslwrap.sh version -a
    ../util/shlib_wrap.sh ./bftest
    ../util/shlib_wrap.sh ./bntest
    ../util/shlib_wrap.sh ./casttest
    ../util/shlib_wrap.sh ./destest
    ../util/shlib_wrap.sh ./dhtest
    ../util/shlib_wrap.sh ./dsatest
    ../util/shlib_wrap.sh ./dummytest
    ../util/shlib_wrap.sh ./ecdhtest
    ../util/shlib_wrap.sh ./ecdsatest
    ../util/shlib_wrap.sh ./ectest
    ../util/shlib_wrap.sh ./enginetest
    ../util/shlib_wrap.sh ./evp_test evptests.txt
    ../util/shlib_wrap.sh ./exptest
    ../util/shlib_wrap.sh ./fips_dssvs
    ../util/shlib_wrap.sh ./fips_test_suite
    ../util/shlib_wrap.sh ./hmactest
    ../util/shlib_wrap.sh ./ideatest
    ../util/shlib_wrap.sh ./igetest
    ../util/shlib_wrap.sh ./md2test
    ../util/shlib_wrap.sh ./md4test
    ../util/shlib_wrap.sh ./md5test
    ../util/shlib_wrap.sh ./randtest
    ../util/shlib_wrap.sh ./rc2test
    ../util/shlib_wrap.sh ./rc4test
    ../util/shlib_wrap.sh ./rmdtest
    ../util/shlib_wrap.sh ./rsa_test
    ../util/shlib_wrap.sh ./sha1test
    ../util/shlib_wrap.sh ./sha256t
    ../util/shlib_wrap.sh ./sha512t
    ../util/shlib_wrap.sh ./shatest
    ../util/shlib_wrap.sh ./ssltest' > run-tests.sh
    rm test/fips_aes_data
    touch test_suite_ready || build_error "error while building test"
}
