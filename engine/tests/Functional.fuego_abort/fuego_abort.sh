#!/bin/bash

set -x

tarball=none

function test_pre_check {
    # if you don't use nohup, ssh transport will hang trying to background fuegosleep
    is_on_target nohup PROGRAM_NOHUP /usr/bin
    assert_define PROGRAM_NOHUP
    echo "This test has pid $$"
}

function test_build {
    touch test_suite_ready
}

function test_deploy {
    true
}

function test_run {
    # leave a process running on the target
    cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR; ln -sf /bin/sleep fuegosleep ; $PROGRAM_NOHUP ./fuegosleep 10000 >/dev/null 2>&1 &"
    report 'echo "Started fuegosleep command from test_run"'
    # leave time here, for user to abort job
    #  1) in Jenkins interface
    #  2) from command line (ftc abort-test)
    #  3) from command line (kill fuego_abort.sh)
    sleep 120
    # now do an internal abort job
    abort_job
}

function test_processing {
    true
}

function more_handler1 {
    echo "in more_handler1 - got another SIGTERM"
    echo "in more_handler1 - got another SIGTERM" >>$LOGFILE
}

function more_handler2 {
    echo "in more_handler2 - got another SIGINT"
    echo "in more_handler2 - got another SIGINT" >>$LOGFILE
}

function more_handler3 {
    echo "in more_handler3 - got another SIGHUP"
    echo "in more_handler3 - got another SIGHUP" >>$LOGFILE
}

function more_handler4 {
    echo "in more_handler4 - got another ERR"
    echo "in more_handler4 - got another ERR" >>$LOGFILE
}

function more_handler5 {
    echo "in more_handler5 - got another EXIT"
    echo "in more_handler5 - got another EXIT" >>$LOGFILE
}

function test_cleanup {
    set +e

    # last chance to put something into the log
    # but have to do it directly
    LOGFILE=${LOGDIR}/testlog.txt

    # check if more traps occur
    trap more_handler1 SIGTERM
    trap more_handler2 SIGINT
    trap more_handler3 SIGHUP
    trap more_handler4 ERR
    trap more_handler5 EXIT

    echo "########" >>$LOGFILE
    echo "Items from here on are from 'test_cleanup' (after the actual test run)" >>$LOGFILE
    echo "this means we are in post_test" >>$LOGFILE

    # do some timing to see how long this lasts:
    for i in $(seq 10) ; do
	    echo "$i delay"
	    echo "$i delay" >>$LOGFILE
	    sleep 1
    done

    # see if fuegosleep is still running on target
    TMPFILE="/tmp/$$-${RANDOM}"
    TMPFILE2="/tmp/$$-${RANDOM}"
    cmd "ps | grep fuegosleep | grep -v grep >$TMPFILE"
    get $TMPFILE $TMPFILE2
    cmd "rm $TMPFILE"

    echo "'fuegosleep' should appear in this log exactly once (except on docker board)" >>$LOGFILE
    echo "before kill_procs:" >>$LOGFILE
    cat $TMPFILE2 >>$LOGFILE
    rm $TMPFILE2

    # try to kill fuegosleep
    kill_procs fuegosleep

    # check if fuegosleep is still running on target
    TMPFILE="/tmp/$$-${RANDOM}"
    TMPFILE2="/tmp/$$-${RANDOM}"
    cmd "ps | grep fuegosleep | grep -v grep >$TMPFILE"
    get $TMPFILE $TMPFILE2
    cmd "rm $TMPFILE"
    echo "after kill_procs:" >>$LOGFILE
    cat $TMPFILE2 >>$LOGFILE
    rm $TMPFILE2

    echo "Leaving test_cleanup"
    echo "Leaving test_cleanup" >>$LOGFILE
    set -e
}

. $FUEGO_CORE/engine/scripts/functional.sh
