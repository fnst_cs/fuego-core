tarball=linpack.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/linpack.c.patch
    $CC $CFLAGS -O -lm -o linpack linpack.c && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put linpack  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR && ./linpack"  
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
