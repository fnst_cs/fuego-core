# some functions are shared between Benchmark.OpenSSL and Functional.OpenSSL
tarball=../OpenSSL/openssl-0.9.8j.tar.gz
source $FUEGO_CORE/engine/tests/OpenSSL/openssl.sh

function test_deploy {
    put apps util test run-tests.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_processing {
    P_CRIT="passed|ok"
    N_CRIT="skip"

    log_compare "$TESTDIR" "176" "${P_CRIT}" "p"
    log_compare "$TESTDIR" "86" "${N_CRIT}" "n"
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; bash run-tests.sh"
}

. $FUEGO_CORE/engine/scripts/functional.sh
