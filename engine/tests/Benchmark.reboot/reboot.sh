tarball=none

function test_build {
	true
}

function test_deploy {
	put $TEST_HOME/get_reboot_time.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	# MAX_REBOOT_RETRIES can be defined in the board's file.
	# Otherwise, the default is 20 retries
	retries=${MAX_REBOOT_RETRIES:-20}
	target_reboot $retries
	# sleep a bit more, just because we're paranoid
	# remote system may have networking up, but need more
	# time to mount filesystems
	sleep 10
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./get_reboot_time.sh"
}

. $FUEGO_CORE/engine/scripts/benchmark.sh
