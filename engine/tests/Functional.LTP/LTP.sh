# FIXTHIS: use https://github.com/linux-test-project/ltp.git instead of a tarball
tarball=ltp-7eb77fbfd80.tar.bz2

ALLTESTS="
admin_tools         fs_perms_simple       ltp-aiodio.part3  net_stress.appl                   quickhit
can                 fs_readonly           ltp-aiodio.part4  net_stress.broken_ip              rpc_tests
cap_bounds          fsx                   ltplite           net_stress.interfaces             sched
commands            hugetlb               lvm.part1         net_stress.ipsec_icmp             scsi_debug.part1
connectors          hyperthreading        lvm.part2         net_stress.ipsec_tcp              securebits
containers          ima                   math              net_stress.ipsec_udp              smack
controllers         input                 mm                net_stress.multicast              stress.part1
cpuhotplug          io                    modules           net_stress.route                  stress.part2
crashme             io_cd                 net.features      network_commands                  stress.part3
dio                 io_floppy             net.ipv6          nptl                              syscalls
dma_thread_diotest  ipc                   net.ipv6_lib      numa                              syscalls-ipc
fcntl-locktests     kernel_misc           net.multicast     nw_under_ns                       timers
filecaps            ltp-aio-stress.part1  net.nfs           pipes                             tirpc_tests
fs                  ltp-aio-stress.part2  net.rpc           power_management_tests            tpm_tools
fs_bind             ltp-aiodio.part1      net.sctp          power_management_tests_exclusive  tracing
fs_ext4             ltp-aiodio.part2      net.tcp_cmds      pty"

ALLPTSTESTS="AIO MEM MSG SEM SIG THR TMR TPS"

ALLRTTESTS="
perf/latency
func/measurement
func/hrtimer-prio
func/gtod_latency
func/periodic_cpu_load
func/pthread_kill_latency
func/sched_football
func/pi-tests
func/thread_clock
func/rt-migrate
func/matrix_mult
func/prio-preempt
func/prio-wake
func/pi_perf
func/sched_latency
func/async_handler
func/sched_jitter
"

function test_pre_check {
    assert_define SDKROOT
    assert_define CC
    assert_define AR
    assert_define RANLIB
    assert_define LDFLAGS
    assert_define FUNCTIONAL_LTP_TESTS
}

function test_build {
    echo "Building LTP"

    # patch the Posix test suite script for easier log processing
    patch -p1 -N -s < $TEST_HOME/0001-pts-merge-all-logfile-files-into-a-pts.log-for-proce.patch

    # Build the LTP tests
    make autotools
    ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS="$LDFLAGS" SYSROOT="${SDKROOT}" \
        --with-open-posix-testsuite --with-realtime-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX \
        --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu
    make CC="${CC}"
    make install

    cp --parents testcases/realtime/scripts/setenv.sh `pwd`/target_bin/
    cp $TEST_HOME/ltp_target_run.sh `pwd`/target_bin/

    touch test_suite_ready
}

function test_deploy {
    echo "Deploying LTP"

    # the syscalls group occupies by far the largest space so remove it if unneeded
    cp_syscalls=$(echo $FUNCTIONAL_LTP_TESTS | awk '{print match($0,"syscalls")}')
    if [ $cp_syscalls -eq 0 ]; then
        echo "Removing syscalls binaries"
        awk '/^[^#]/ { print "rm -f target_bin/testcases/bin/" $2 }' target_bin/runtest/syscalls | sh
    fi

    put target_bin/*  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    echo "Running LTP"

    TESTS=""
    PTSTESTS=""
    RTTESTS=""

    for a in $FUNCTIONAL_LTP_TESTS; do
        for b in $ALLTESTS; do
            if [ "$a" == "$b" ]; then
                TESTS+="$a "
            fi
        done

        for b in $ALLPTSTESTS; do
            if [ "$a" == "$b" ]; then
                PTSTESTS+="$a "
            fi
        done

        for b in $ALLRTTESTS; do
            if [ "$a" == "$b" ]; then
                RTTESTS+="$a "
            fi
        done
    done

    # Let some of the tests fail, the information will be in the result xlsx file
    set +e
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; TESTS=\"$TESTS\"; PTSTESTS=\"$PTSTESTS\"; RTTESTS=\"$RTTESTS\"; . ./ltp_target_run.sh"
    set -e
}

function test_processing {
    echo "Processing LTP"

    get $BOARD_TESTDIR/fuego.$TESTDIR/result .
    cd result/
    # Restore the path so that python works properly
    export PATH=$ORIG_PATH
    cp $TEST_HOME/ltp_process.py .
    python ltp_process.py
    [ -e results.xlsx ] && cp results.xlsx ${LOGDIR}/results.xlsx
    [ -e rt.log ] && cp rt.log $FUEGO_RW/logs/$JOB_NAME/
    cd ..
    rm -rf result/
}

. $FUEGO_CORE/engine/scripts/functional.sh
