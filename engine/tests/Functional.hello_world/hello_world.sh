#!/bin/bash

tarball=hello-test-1.1.tgz

function test_build {
	make && touch test_suite_ready || build_error "error while building test"
}

function test_deploy {
	put hello  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
#    assert_define FUNCTIONAL_HELLO_WORLD_ARG
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./hello $FUNCTIONAL_HELLO_WORLD_ARG"
}

function test_processing {
    log_compare "$TESTDIR" "1" "SUCCESS" "p"
}

. $FUEGO_CORE/engine/scripts/functional.sh
