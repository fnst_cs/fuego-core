{
    "testName": "Benchmark.IOzone",
    "specs":
    [
        {
            "name":"sata",
            "MOUNT_BLOCKDEV":"$SATA_DEV",
            "MOUNT_POINT":"$SATA_MP",
            "FILE_SIZE":"2M",
            "TESTS":"'-i 0 -i 1 -i 2 -i 6 -i 7 -i 8 -i 9'"
        },
        {
            "name":"mmc",
            "MOUNT_BLOCKDEV":"$MMC_DEV",
            "MOUNT_POINT":"$MMC_MP",
            "FILE_SIZE":"2M",
            "TESTS":"'-i 0 -i 1 -i 2 -i 6 -i 7 -i 8 -i 9'"
        },
        {
            "name":"usb",
            "MOUNT_BLOCKDEV":"$USB_DEV",
            "MOUNT_POINT":"$USB_MP",
            "FILE_SIZE":"2M",
            "TESTS":"'-i 0 -i 1 -i 2 -i 6 -i 7 -i 8 -i 9'"
        },
        {
            "name":"nopread",
            "MOUNT_BLOCKDEV":"ROOT",
            "MOUNT_POINT":"$BOARD_TESTDIR/work",
            "FILE_SIZE":"2M",
            "TESTS":"'-i 0 -i 1 -i 2 -i 6 -i 7 -i 8'"
        },
        {
            "name":"default",
            "MOUNT_BLOCKDEV":"ROOT",
            "MOUNT_POINT":"$BOARD_TESTDIR/work",
            "FILE_SIZE":"2M",
            "TESTS":"'-i 0 -i 1 -i 2 -i 6 -i 7 -i 8 -i 9'"
        }
    ]
}




